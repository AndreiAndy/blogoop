<?php
include 'functions query.php';
?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<div class="container" >
    <div class="row" id="header">
        <div class="col-3">
            <a href="index.php"><img src="photos/ad.jpg" width="100"/></a>
        </div>
        <div class="col-9">
            <h1 style="color:darkblue;font-family: 'Bauhaus 93'">Blogul meu</h1>
        </div>
    </div>
    <div id="menu" class="navbar navbar-dark bg-dark">
        <?php
        $categories = Category::findBy();
        foreach ($categories as $category) {
            echo '<li><a href="category.php?id=' . $category->getId() . '">' . $category->getName() . '</a></li>';
        }
        echo '<a href="createPost.php"><h3 style="color:green">+Add Post</h3></a>'; ?>
    </div>
<form action="processCreatePost.php" method="post">
    <div class="form-group">
    <label for="titleInput">Title</label>
    <input id="titleInput"  class="form-control" type="text" name="title"/>
    </div>
    <div class="form-group">
    <label for="textInput">Text</label>
    <input id="textInput" class="form-control" type="text" name="template"/>
    </div>
    <div class="form-group">
    <label for="category">Category</label>
    <select class="form-control" name="category">
       <?php $categories=Category::findBy();
       foreach ($categories as $category) {
           echo '<option value="' . $category->getId() . '">' . $category->getName() . '</option>';
       }
        ?>
    </select>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
    <div id="footer" class="container-fluid text-center" style="background-color: lightslategray">
        <p >&copy; 2020</p>
    </div>
</div>