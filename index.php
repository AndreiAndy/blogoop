<?php include "functions query.php"; ?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<body>
<div class="container" >
    <div class="row" id="header">
        <div class="col-3">
            <a href="index.php"><img src="photos/ad.jpg" width="100"/></a>
        </div>
        <div>
            <h1 style="color:darkblue;font-family: 'Bauhaus 93'">Blogul meu</h1>
        </div>
    </div>
    <div id="menu" class="navbar navbar-dark bg-dark">
        <?php
        $categories = Category::findBy();
        foreach ($categories as $category) {
            echo '<li><a href="category.php?id=' . $category->getId() . '">' . $category->getName() . '</a></li>';
        }
        echo '<a href="createPost.php"><h3 style="color:green">+Add Post</h3></a>'; ?>
    </div>
    <div id="content" class="container-fluid" style="background-color: royalblue; border: solid ">
        <?php
        $currentPage = 1;
        if (isset($_GET['p'])) {
            $currentPage = $_GET['p'];
        }
        $posts = Post::findBy([], 'id', 'DESC', 2, ($currentPage - 1) * 2);


        foreach ($posts as $post):?>
            <div class="card mt-3">
                <img class="card-img-top" src="photos/<?php echo $post->getPhoto() ?>" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $post->getTitle() ?></h5>
                    <p class="card-text"><?php echo $post->limitedContent() ?></p>
                    <a href="post.php?id=<?php echo $post->getId() ?>" class="btn btn-primary">Read more</a>
                    <a href="deletePost.php?id=' .$post->getId().'"><h4 style="color:red">Delete</h4></a>
                </div>
            </div>
        <?php endforeach; ?>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <?php if ($currentPage > 1): ?>
                    <li class="page-item"><a class="page-link" href="index.php">&laquo;</a></li>
                    <li class="page-item"><a class="page-link"
                                             href="index.php?p=<?php echo $currentPage - 1; ?>">&lt;</a>
                    </li>
                <?php endif; ?>
                <li class="page-item"><a class="page-link"
                                         href="index.php?p=<?php echo $currentPage; ?>"><?php echo $currentPage; ?></a>
                </li>
                <li class="page-item"><a class="page-link" href="index.php?p=<?php echo $currentPage + 1; ?>">&gt;</a>
                </li>
            </ul>
        </nav>
    </div>
<div id="footer" class="container-fluid text-center" style="background-color: lightslategray">
    <p >&copy; 2020</p>
</div>
    </div>
</body>
