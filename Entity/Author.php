<?php

/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 4/14/2020
 * Time: 12:41 PM
 */
class Author extends Base
{
 public $name;

 public $surname;

 public function getPosts()
 {
     return Post::findBy(['author_id'=>intval($this->getId())]);

 }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Author
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     * @return Author
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }
}