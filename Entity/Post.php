<?php

/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 2/24/2020
 * Time: 8:25 PM
 */
class Post extends Base
{
    public $title;

    public $template;

    public $photo;

    public $category_id;

    public $author_id;

    public function limitedContent($length = 400){
        if (strlen($this->template) < $length){
            return $this->template;
        } else {
            return substr($this->template, 0, strpos($this->template, ' ', $length)).'...';
        }

    }
    public function getCategory(){
        return Category::find($this->getCategoryId());
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $category_id
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * @return mixed
     */
    public function getAuthorId()
    {
        return $this->author_id;
    }

    /**
     * @param mixed $authors_id
     * @return Post
     */
    public function setAuthorId($author_id)
    {
        $this->author_id = $author_id;
        return $this;
    }


}