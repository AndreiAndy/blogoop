<?php

/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 2/26/2020
 * Time: 8:41 PM
 */
class Category extends Base
{
public $name;

    public function getPosts()
    {
        return Post::findBy(['category_id'=>intval($this->getId())]);
    }
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}
